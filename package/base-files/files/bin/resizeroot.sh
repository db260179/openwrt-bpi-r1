#!/bin/ash
###################################################################
# Purpose:
# This is a simple script which looks at the current disk that is
# being used and expands the filesystem to almost the max 
# minus 3 512 blocks.  This is the ensure that the image is
# smaller than most SDcards of that size
#
# Instructions:
# It is pretty much automatic... 
# it performs a resize command and setups a script which will 
# run after a reboot.
#
# The script WILL REBOOT YOUR SYSTEM
#
# When the system is coming back up, the next command will run
# automatically, and the one time script will be removed and
# when you see the login prompt again, it will be complete
# Packages: parted, fdisk and blockdev need to be installed in the image.
###################################################################
# START OF SCRIPT
###################################################################
PROGRAM="openwrt-resizeSD-root"
VERSION="v1.0"
###################################################################

DISK_SIZE="$(($(blockdev --getsz /dev/mmcblk0)/2048/925))"
PART_START="$(parted /dev/mmcblk0 -ms unit s p |grep "^2" |cut -f2 -d:|sed s'/s//')"
[ "$PART_START" ] || exit 1
PART_END="$(((DISK_SIZE*925*2048-1)-1536))"
###################################################################
# Display some Stuff...
###################################################################
echo $PROGRAM - $VERSION
echo ======================================================
echo Current Disk Info
fdisk -l /dev/mmcblk0
echo
echo ======================================================
echo
echo Calculated Info:
echo " Disk Size  = $DISK_SIZE gb"
echo " Part Start = $PART_START"
echo " Part End   = $PART_END"
echo
echo "Making changes using fdisk..."
printf "d\n2\nn\np\n2\n$PART_START\n$PART_END\np\nw\n" | fdisk /dev/mmcblk0
echo
echo Setting up init.d resize2fs_once script

cat <<EOF > /etc/init.d/resize2fs_once
#!/bin/sh /etc/rc.common

START=11

start()
{
    echo "Starting resize2fs_once, THIS WILL TAKE A FEW MINUTES "
    
    # Do our stuff....   
    resize2fs /dev/mmcblk0p2
    
    # Okay, lets disable and remove this script
    /etc/init.d/resize2fs_once disable
    rm /etc/init.d/resize2fs_once
}
EOF
  chmod +x /etc/init.d/resize2fs_once
  /etc/init.d/resize2fs_once enable

echo
echo #####################################################################
echo System is now ready to resize your system.  A REBOOT IS REQUIRED NOW!
echo "Press ENTER to reboot : \c"
read aok
echo REBOOTING....
touch /forcefsck
/bin/sync
/sbin/reboot
echo
echo Script Complete...

###################################################################
# END OF SCRIPT
###################################################################

